FROM docker.io/library/archlinux:latest

SHELL ["/bin/bash", "-c"]

ADD add_files/freecad_build_script.sh /root/build_script.sh
RUN pacman -Syu --noconfirm vim boost curl desktop-file-utils glew hicolor-icon-theme jsoncpp libspnav opencascade shiboken2 xerces-c pyside2 python-matplotlib python-netcdf4 qt5-svg qt5-webkit qt5-webengine cmake eigen git gcc-fortran pyside2-tools swig qt5-tools shared-mime-info coin python-pivy med make openmpi
WORKDIR /root

# Note for later: May need -fPIC
