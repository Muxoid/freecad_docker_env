#!/bin/bash

set -e

cmake -DBUILD_QT5=ON -DPYTHON_EXECUTABLE=/usr/bin/python3 -DCMAKE_BUILD_TYPE=Release -S /mnt/source -B /mnt/build

cd /mnt/build

make -j $(nproc)
